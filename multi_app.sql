-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Dec 05, 2017 at 07:41 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multi_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(17) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `full_name`, `email`, `mobile`, `dob`, `created`, `modified`) VALUES
(2, 4, 'eather', 'eather.ahmed@gmail.com', NULL, NULL, '2017-12-05 18:48:59', '2017-12-05 18:48:59'),
(3, 5, 'Admin', 'admin@gmail.com', NULL, NULL, '2017-12-05 18:49:22', '2017-12-05 18:49:22'),
(4, 6, 'Admin', 'admin1@gmail.com', NULL, NULL, '2017-12-05 18:49:56', '2017-12-05 18:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `active_status` tinyint(4) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `active_status`, `user_type`, `created`, `modified`) VALUES
(4, 'eather.ahmed@gmail.com', '$2y$10$/OIAv5R.BOoTLNqCxBPFzOupttzSspjxWaKpstmXeAAv9f/CD20Yq', 1, 2, '2017-12-05 18:48:59', '2017-12-05 19:18:09'),
(5, 'admin@gmail.com', '$2y$10$/OIAv5R.BOoTLNqCxBPFzOupttzSspjxWaKpstmXeAAv9f/CD20Yq', 1, 1, '2017-12-05 18:49:22', '2017-12-05 19:18:10'),
(6, 'admin1@gmail.com', '$2y$10$/OIAv5R.BOoTLNqCxBPFzOupttzSspjxWaKpstmXeAAv9f/CD20Yq', 1, 1, '2017-12-05 18:49:56', '2017-12-05 19:18:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `type_index` (`user_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
