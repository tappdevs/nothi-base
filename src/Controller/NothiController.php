<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class NothiController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function index()
    {

    }

    public function nothi()
    {
      $this->viewBuilder()->setLayout('default-full');
      $potros_group_nav_all = json_encode(array(
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'word', //button image/icon
//            'file_name' => $this->request->webroot . 'coreui/docs/pdf.pdf',
//            'file_name' => $this->request->webroot . 'coreui/docs/excel.xlsx',
              'file_name' => $this->request->webroot . 'coreui/docs/pdf.pdf',
//            'file_name' => $this->request->webroot . 'coreui/docs/docs.docx',
//            'file_name' => $this->request->webroot . 'coreui/docs/E-NothiRFP.pdf',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image', //button image/icon
              'file_name' => $this->request->webroot . 'coreui/docs/docs.docx',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '৩', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 3,
              'attachment_type' => 'pdf', //button image/icon
              'file_name' => $this->request->webroot . 'coreui/docs/excel.xlsx',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '৪', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৮.০১.০০১.১১.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 1,
              'is_summary_nothi' => 1,
              'is_approved' => 1,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '৫', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'word', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '৬', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '৭', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '৮', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 3,
              'attachment_type' => 'image/png', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701021.png',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '৯', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৮.০১.০০১.১১.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 1,
              'is_summary_nothi' => 1,
              'is_approved' => 1,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '১০', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '১১', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '১২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 3,
              'attachment_type' => 'image/png', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701021.png',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '১৪', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৮.০১.০০১.১১.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 1,
              'is_summary_nothi' => 1,
              'is_approved' => 1,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        ),
        array(
          'potro_group_status' => 1,
          'potro_count' => 5,
          'potros' => array(
            array(
              'attachment_id' => 1,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '১৫', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 1,
              'attachment_type' => 'pdf', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701072.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.৩', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'web',
              'created' => '2018-11-20 16:19:09',
            ),
            array(
              'attachment_id' => 2,
              'attachment_type' => 'image/jpeg', //button image/icon
              'file_name' => 'Dak/65/2018/11/20/1-1542701073.jpg',
              'file_dir' => 'http://nothi.tappware.com/content/',
              'nothi_potro_page_bn' => '২', //to dispaly the number on view
              'sarok_no' => '০৩.০৮.০০০০.৮০৫.০১.০০১.১৮.১', //tooltip on hover
              'attachment_description' => '', //tooltip on hover with sarok_no
              'potro_no' => 1,
              'application_origin' => 'bsap',
              'is_nothijato' => 0,
              'is_main' => 1,
              'is_potrojari' => 0,
              'potrojari_status' => 0,
              'is_summary_nothi' => 0,
              'is_approved' => 0,
              'status' => 1,
              'device_type' => 'mobile',
              'created' => '2018-11-20 16:19:09',
            ),
          )
        )
      ));
      $potros_group_nav_all = json_decode($potros_group_nav_all);

      $this->set(compact('potros_group_nav_all'));


    }

}
