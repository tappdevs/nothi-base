<?php

namespace App\Model\Custom;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Cake\Core\Configure;

Class TappEmail {

    function sendRegistrationEmail($data = []) {
        if (!empty($data) && defined("EMAIL_QUEUE_URL")) {
            $ta_email_template = \Cake\ORM\TableRegistry::get('EmailTemplates');
            $account_email = $ta_email_template->find()->where(['email_type' => 'registration'])->first();

            $email_body = $account_email['email_body'];
            $email_body = str_replace("KEY_VERIFICATION_CODE", $data['verification_otp'],
                    $email_body);
            $email_body = str_replace("KEY_VERIFICATION_LINK",
                $data['verification_link'], $email_body);
            return $this->emailCurl(
                    ['to'=>$data['email'], 
                        'email_body'=>$email_body, 
                        'email_subject'=>$account_email['mail_title']]);
        } else {
            return ['status'=>0, 'msg'=>__("Failed to add email in queue")];
        }
    }
    
    function sendActivationEmail($data = []) {
        if (!empty($data) && defined("EMAIL_QUEUE_URL")) {
            $ta_email_template = \Cake\ORM\TableRegistry::get('EmailTemplates');
            $account_email = $ta_email_template->find()->where(['email_type' => 'activation'])->first();

            $email_body = $account_email['email_body'];

            return $this->emailCurl(
                    ['to'=>$data['email'], 
                        'email_body'=>$email_body,
                        'email_subject'=>$account_email['mail_title']]);
            
        } else {
            return ['status'=>0, 'msg'=>__("Failed to add email in queue")];
        }
    }

    function sendConfirmationEmail($data = []) {
        if (!empty($data) && defined("EMAIL_QUEUE_URL")) {
            $ta_email_template = \Cake\ORM\TableRegistry::get('EmailTemplates');
            $account_email = $ta_email_template->find()->where(['email_type' => 'confirmation'])->first();

            $email_body = $account_email['email_body'];
            $email_body = str_replace("KEY_LOGIN_URL", $data['login_url'],
                $email_body);
            $email_body = str_replace("KEY_LOGIN_USER", $data['user'],
                $email_body);
            $email_body = str_replace("KEY_LOGIN_PASSWORD", $data['pass'],
                $email_body);

            return $this->emailCurl(
                ['to'=>$data['email'],
                    'email_body'=>$email_body,
                    'email_subject'=>$account_email['mail_title']]);
        } else {
            return ['status'=>0, 'msg'=>__("Failed to add email in queue")];
        }
    }
    
    function emailCurl($data){
        $curl = curl_init();
        curl_setopt_array($curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => EMAIL_QUEUE_URL,
                CURLOPT_USERAGENT => Configure::read('APP_NAME').' Email Queue',
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => array(
                    'service_id' => 10,
                    'email_trace_id' => 0,
                    'email_type' => 0,
                    'email_type_label' => "",
                    'email_body' => $data['email_body'],
                    'email_from' => FROM_EMAIL,
                    'email_from_name' => Configure::read('APP_NAME'),
                    'email_subject' => $data['email_subject'],
                    'email' => $data['to'],
                    'name' => "",
                    'response_url' => ""
                )
        ));
        try{
            curl_exec($curl);
            curl_close($curl);
            return ['status'=>1, 'msg'=>SUCCESS_MSG];
        }
        catch(\Cake\Core\Exception\Exception $e){
            curl_close($curl);
            return ['status'=>0, 'msg'=>$e->getMessage()];
        }
    }
}
