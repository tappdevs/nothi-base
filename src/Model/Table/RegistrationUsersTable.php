<?php
namespace App\Model\Table;

use App\Model\Custom\TappEmail;
use Cake\Core\Configure;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
class RegistrationUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('registration_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('email')
            ->maxLength('email', 255)
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
        $validator
            ->scalar('account_name')
            ->maxLength('account_name', 255)
            ->requirePresence('account_name', 'create')
            ->notEmpty('account_name')
            ->add('account_name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    public function saveUser($rd){
        $rd['domain'] = $rd['domain_prefix'].$rd['domain_postfix'];
        $validation_response = json_decode($this->validateUser($rd), true);
        if($validation_response['status'] == 0) {
            return $validation_response;
        }
        $rd['verification_otp'] = generateRandomString();
        $response = $this->modifiedSave($rd);
        if($response['status'] == 1) {
            $tapp_email = new TappEmail();
            return $tapp_email->sendRegistrationEmail($rd);
        }
        else {
            $error = $response['msg'];
            foreach($response['errors'] as $field=>$errors){
                foreach($errors as $key=>$value){
                    $error = $field. " - [ ".$key." ] "." - ".$value;
                    break 2;
                }
            }
            return ['status'=>0, 'msg'=>$error];
        }
    }

    public function verify($rd){
        $user = $this->find()->where(['email' => $rd['email'], 'verification_otp' => $rd['verification_otp']])
            ->enableHydration(false)->first();
        if (empty($user)) {
            return ['status'=>0, 'msg'=>'Invalid user'];
        } else if ($user['active_status'] == 1) {
            return ['status'=>0, 'msg'=>'Already verified'];
        } else if ($user['active_status'] == 2) {
            return ['status'=>0, 'msg'=>'Already verified and account activated'];
        } else {
            $user['active_status'] = 1;
            $response = $this->modifiedSave($user);
            if($response['status'] == 1) {
                $tapp_email = new TappEmail();
                return $tapp_email->sendActivationEmail($user);
            }
            return $response;
        }
    }

    public function validateUser($rd){
        $curl = curl_init();
        curl_setopt_array($curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => USER_VALIDATION_URL,
                CURLOPT_USERAGENT => Configure::read('APP_NAME').' Validate User',
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $rd
            ));
        try {
            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        }
        catch(\Cake\Core\Exception\Exception $e){
            curl_close($curl);
            return ['status'=>0, 'msg'=>$e->getMessage()];
        }
    }
}
