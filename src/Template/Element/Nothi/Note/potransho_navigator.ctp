<div class="note_navigation overflow-hidden">
  <div id="<?= $potros_nav_id ?>" class="slides d-flex justify-content-between">
    <button class="btn btn-label-warning note_left btn-icon btn-square"><i class="fa fa-angle-left"></i></button>
    <div class="overflow-hidden mx-1 flex-fill">
      <div class="btn-toolbar flex-nowrap nav navWidth" role="tablist">
        <?php
        $i = 0;
        foreach ($potros_group_nav as $group_key => $potros_groups) {
          $i += 1;
          ?>
          <div class="btn-group flex-nowrap mr-2 nav<?= $i == 1 ? ' topSet' : ''; ?>" role="group" aria-label="First group">
            <?php foreach ($potros_groups->potros as $potro_key => $potros_group) {
              $tooltip = 'page_bn: ' . $potros_group->nothi_potro_page_bn;
              $tooltip .= 'sarok_no: ' . $potros_group->sarok_no;
              $tooltip .= 'attachment_description: ' . $potros_group->attachment_description;
              $tooltip .= 'potro_no: ' . $potros_group->potro_no;
              $tooltip .= 'application_origin: ' . $potros_group->application_origin;
              $tooltip .= 'is_nothijato: ' . $potros_group->is_nothijato;
              $tooltip .= 'is_main: ' . $potros_group->is_main;
              $tooltip .= 'is_potrojari: ' . $potros_group->is_potrojari;
              $tooltip .= 'potrojari_status: ' . $potros_group->potrojari_status;
              $tooltip .= 'is_summary_nothi: ' . $potros_group->is_summary_nothi;
              $tooltip .= 'is_approved: ' . $potros_group->is_approved;
              $tooltip .= 'status: ' . $potros_group->status;
              $tooltip .= 'device_type: ' . $potros_group->device_type;
              $tooltip .= 'created: ' . $potros_group->created;
              $attachment_type = 'fas fa-list-alt';

              if ($potros_group->attachment_type == 'pdf') {
                $attachment_type = 'fas fa-file-pdf';
              }
              if ($potros_group->attachment_type == 'image') {
                $attachment_type = 'fa fa-image';
              }
              if ($potros_group->attachment_type == 'word') {
                $attachment_type = 'fas fa-file-word';
              }
              ?>
              <button href="#tab<?= $group_key . $potro_key ?>"
                  class="btn btn-label-info btn-icon position-relative"
                  type="button">
                <i class="<?= $attachment_type ?>"></i>
                <span style="position:absolute;top:0; right:0;padding:1px 2px;"
                    class="badge badge-warning text-white"><?= $potros_group->nothi_potro_page_bn ?></span>
              </button>
            <?php } ?>
          </div>
        <?php } ?>


      </div>
    </div>
    <button class="btn btn-label-warning note_left btn-icon btn-square"><i class="fa fa-angle-right"></i></button>
  </div>
</div>
<script>
  $(function(e){
    console.log('potro navigation');
    $('.note_left').on('click',function (e) {
      var navWidth = $('.navWidth').width();
      console.log(navWidth);
      $('.btn-toolbar.navWidth')css('left', '-'+navWidth);
    })

  })

</script>
