

<div class="onucched_numbers h-100" data-scroll="true">
  <i class="fa fa-times togg-close"></i>
  <div class="accordion text-center" id="accordionExample">
    <div class="card mb-0">
      <button class="btn btn-light dropdown-toggle collapsed" type="button" data-toggle="collapse"
        data-target="#onuNo1" aria-expanded="false" aria-controls="collapseOne">
        1
      </button>

      <div id="onuNo1" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample"
        style="">
        <div class="list-group list-group-flush border-top">
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">1.0</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">1.2</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">1.3</a>
        </div>
      </div>
    </div>
    <div class="card mb-0">
      <button class="btn btn-light dropdown-toggle collapsed" type="button" data-toggle="collapse"
        data-target="#onuNo2" aria-expanded="false" aria-controls="collapseOne">
        2
      </button>

      <div id="onuNo2" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample"
        style="">
        <div class="list-group list-group-flush border-top">
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">2.0</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">2.2</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">2.3</a>
        </div>
      </div>
    </div>
    <div class="card mb-0">
      <button class="btn btn-light dropdown-toggle collapsed" type="button" data-toggle="collapse"
        data-target="#onuNo3" aria-expanded="false" aria-controls="collapseOne">
        3
      </button>

      <div id="onuNo3" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample"
        style="">
        <div class="list-group list-group-flush border-top">
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">3.0</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">3.2</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">3.3</a>
        </div>
      </div>
    </div>
    <div class="card mb-0">
      <button class="btn btn-light dropdown-toggle collapsed" type="button" data-toggle="collapse"
        data-target="#onuNo4" aria-expanded="false" aria-controls="collapseOne">
        4
      </button>

      <div id="onuNo4" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample"
        style="">
        <div class="list-group list-group-flush border-top">
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">4.0</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">4.2</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">4.3</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">4.4</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">4.5</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">4.6</a>
        </div>
      </div>
    </div>
    <div class="card mb-0">
      <button class="btn btn-light dropdown-toggle collapsed" type="button" data-toggle="collapse"
        data-target="#onuNo5" aria-expanded="false" aria-controls="collapseOne">
        5
      </button>

      <div id="onuNo5" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample"
        style="">
        <div class="list-group list-group-flush border-top">
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.0</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.2</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.3</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.4</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.5</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.6</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.2</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.3</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.4</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.5</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.6</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.2</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.3</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.4</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.5</a>
          <a href="#" class="list-group-item list-group-item-action p-2 list-group-item-info">5.6</a>
        </div>
      </div>
    </div>
  </div>
</div>


<!--onucched:start-->
<div class="card border-0 h-100 mb-0 notansho_note_view">

  <div class="card-header">
    <h4 class="mb-0 text-truncate note_title">
      অনুচ্ছেদ: নোট এর শিরোনাম
    </h4>
    <h6 class="mb-0 note_datetime">২০-০৩-২০১৯</h6>
  </div>

  <div class="card-body overflow-hidden flex-basis p-0 d-flex">

    <div class="onucched_content p-3 w-100" data-scroll="true">
      <div class="onucched_list">

        <ul class="list-group">
          <li class="list-group-item list-group-item-light p-0 mb-3">
            <div class="d-flex justify-content-between align-items-center border-bottom px-2 py-1 onucched_heading">
              <h5 class="mb-0">অনুচ্ছেদ ১.০
                <small>(১৯-০২-২০১৯ ০৪:১৮:৪৯ অপরাহ্ণ)</small>
              </h5>
              <div class="btn-group " role="group" aria-label="Basic example">
                <button type="button"
                  class="btn btn-label-primary btn-sm shadow-none btn-icon">
                  <i class="fs1 a2i_nt_dakdraft4"></i>
                </button>
                <button type="button" class="btn btn-label-success btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_edit2"></i>
                </button>
                <button type="button" class="btn btn-label-danger btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_delete2 "></i>
                </button>
              </div>
            </div>
            <div class="p-2 bg-white">
              <div class="printArea">
                <div class="note_creation_time">২৩-০২-২০১৯ ১০:২৯:৪৫ পূর্বাহ্ণ</div>
                <span class="noteNo"> অনুচ্ছেদ: ১.২</span>
                <div class="noteDescription" id="116">অনুচ্ছেদ <a
                    href="http://hasan/tappware/nothi/noteDetail/46" target="__tab" title=""
                    data-original-title="নোট">&nbsp;১ *</a>, &nbsp;বিবেচ্য গার্ড ফাইল <a
                    class="showforPopup" href="7/guard-attachment/6/2" title=""
                    data-original-title=" গার্ড ফাইল">[০১-০৭-২০১৫]দপ্তরাদেশ- এন বিবেচ্য গার্ড
                    ফাইল&nbsp;</a><a class="showforPopup" href="7/guard-attachment/6/3" title=""
                    data-original-title=" গার্ড ফাইল">[০১-০৭-২০১৫]দপ্তরাদেশ- এনজিএফএফ (পৃষ্ঠা ৩)</a>,
                  &nbsp;জিএফএফ (পৃষ্ঠা ২), &nbsp;<br><br>বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="5/guard-attachment/4/9" title="" data-original-title=" গার্ড ফাইল">test 4
                    (পৃষ্ঠা ৯)</a>, &nbsp; বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="5/guard-attachment/4/7" title="" data-original-title=" গার্ড ফাইল">test 4
                    (পৃষ্ঠা ৭)</a>, &nbsp;<br><br>বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="6/guard-attachment/5/5" title="" data-original-title=" গার্ড ফাইল">সরকারী
                    সম্পত্তির বীমা (পৃষ্ঠা ৫)</a>, &nbsp; বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="6/guard-attachment/5/6" title="" data-original-title=" গার্ড ফাইল">সরকারী
                    সম্পত্তির বীমা (পৃষ্ঠা ৬)</a>, &nbsp;<br>&nbsp;বিবেচ্য গার্ড ফাইল &nbsp;<a
                    class="showforPopup" href="7/guard-attachment/6/5" title=""
                    data-original-title=" গার্ড ফাইল">[০১-০৭-২০১৫]দপ্তরাদেশ- এনজিএফএফ (পৃষ্ঠা ৫)</a>,
                  &nbsp;
                </div>
              </div>
              <hr>
              <div class="note_attachment">
                <table class="table table-bordered">
                  <tbody>
                  <tr class="alert-warning">
                    <td colspan="4">সংযুক্তি</td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td><a href="#">asdfdsdfs.jpg</a></td>
                    <td>Lorem ipsum dolor</td>
                    <td><a href="#"><i class="fa fa-download"></i></a></td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <hr>

              <div class="note_signature">
                <div class="card-columns">
                  <div class="card text-center">
                    <img class="card-img-top border-bottom" src="<?= $this->request->webroot?>img/sign.png"
                      alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top border-bottom" src="<?= $this->request->webroot?>img/sign.png"
                      alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top border-bottom" src="<?= $this->request->webroot?>img/sign.png"
                      alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top" src="<?= $this->request->webroot?>img/sign.png" alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top" src="<?= $this->request->webroot?>img/sign.png" alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </li>
          <li class="list-group-item list-group-item-light p-0 mb-3 text-danger">
            <div class="d-flex justify-content-between align-items-center border-bottom px-2 py-1 onucched_heading">
              <h5 class="mb-0">অনুচ্ছেদ ১.০
                <small>(১৯-০২-২০১৯ ১২:১৮:৪৯ অপরাহ্ণ)</small>
              </h5>
              <div class="btn-group " role="group" aria-label="Basic example">
                <button type="button"
                    class="btn btn-label-primary btn-sm shadow-none btn-icon">
                  <i class="fs1 a2i_nt_dakdraft4"></i>
                </button>
                <button type="button" class="btn btn-label-success btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_edit2"></i>
                </button>
                <button type="button" class="btn btn-label-danger btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_delete2 "></i>
                </button>
              </div>
            </div>
            <div class="p-2 bg-white">
              <div class="printArea">
                <div class="note_creation_time">১৯-০২-২০১৯ ০৪:১৯:০৫ অপরাহ্ণ</div>
                <span class="noteNo"> অনুচ্ছেদ: ১.১</span>
                <div class="noteDescription" id="114">বিকল্প প্রস্তাব দিন পরিচ্ছন্ন পত্রসহ উপস্থাপন করুন
                </div>
              </div>
            </div>
          </li>
          <li class="list-group-item list-group-item-light p-0 mb-3">
            <div class="d-flex justify-content-between align-items-center border-bottom px-2 py-1 onucched_heading">
              <h5 class="mb-0">অনুচ্ছেদ ১.০
                <small>(১৯-০২-২০১৯ ০৪:১৮:৪৯ অপরাহ্ণ)</small>
              </h5>
              <div class="btn-group " role="group" aria-label="Basic example">
                <button type="button"
                    class="btn btn-label-primary btn-sm shadow-none btn-icon">
                  <i class="fs1 a2i_nt_dakdraft4"></i>
                </button>
                <button type="button" class="btn btn-label-success btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_edit2"></i>
                </button>
                <button type="button" class="btn btn-label-danger btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_delete2 "></i>
                </button>
              </div>
            </div>
            <div class="p-2 bg-white">
              <div class="printArea">
                <div class="note_creation_time">২৩-০২-২০১৯ ১০:২৯:৪৫ পূর্বাহ্ণ</div>
                <span class="noteNo"> অনুচ্ছেদ: ১.২</span>
                <div class="noteDescription" id="116">অনুচ্ছেদ <a
                    href="http://hasan/tappware/nothi/noteDetail/46" target="__tab" title=""
                    data-original-title="নোট">&nbsp;১ *</a>, &nbsp;বিবেচ্য গার্ড ফাইল <a
                    class="showforPopup" href="7/guard-attachment/6/2" title=""
                    data-original-title=" গার্ড ফাইল">[০১-০৭-২০১৫]দপ্তরাদেশ- এন বিবেচ্য গার্ড
                    ফাইল&nbsp;</a><a class="showforPopup" href="7/guard-attachment/6/3" title=""
                    data-original-title=" গার্ড ফাইল">[০১-০৭-২০১৫]দপ্তরাদেশ- এনজিএফএফ (পৃষ্ঠা ৩)</a>,
                  &nbsp;জিএফএফ (পৃষ্ঠা ২), &nbsp;<br><br>বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="5/guard-attachment/4/9" title="" data-original-title=" গার্ড ফাইল">test 4
                    (পৃষ্ঠা ৯)</a>, &nbsp; বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="5/guard-attachment/4/7" title="" data-original-title=" গার্ড ফাইল">test 4
                    (পৃষ্ঠা ৭)</a>, &nbsp;<br><br>বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="6/guard-attachment/5/5" title="" data-original-title=" গার্ড ফাইল">সরকারী
                    সম্পত্তির বীমা (পৃষ্ঠা ৫)</a>, &nbsp; বিবেচ্য গার্ড ফাইল <a class="showforPopup"
                    href="6/guard-attachment/5/6" title="" data-original-title=" গার্ড ফাইল">সরকারী
                    সম্পত্তির বীমা (পৃষ্ঠা ৬)</a>, &nbsp;<br>&nbsp;বিবেচ্য গার্ড ফাইল &nbsp;<a
                    class="showforPopup" href="7/guard-attachment/6/5" title=""
                    data-original-title=" গার্ড ফাইল">[০১-০৭-২০১৫]দপ্তরাদেশ- এনজিএফএফ (পৃষ্ঠা ৫)</a>,
                  &nbsp;
                </div>
              </div>
              <hr>
              <div class="note_attachment">
                <table class="table table-bordered">
                  <tbody>
                  <tr class="alert-warning">
                    <td colspan="4">সংযুক্তি</td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td><a href="#">asdfdsdfs.jpg</a></td>
                    <td>Lorem ipsum dolor</td>
                    <td><a href="#"><i class="fa fa-download"></i></a></td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <hr>

              <div class="note_signature">
                <div class="card-columns">
                  <div class="card text-center">
                    <img class="card-img-top border-bottom" src="<?= $this->request->webroot?>img/sign.png"
                      alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top border-bottom" src="<?= $this->request->webroot?>img/sign.png"
                      alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top border-bottom" src="<?= $this->request->webroot?>img/sign.png"
                      alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top" src="<?= $this->request->webroot?>img/sign.png" alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                  <div class="card text-center">
                    <img class="card-img-top" src="<?= $this->request->webroot?>img/sign.png" alt="sign">
                    <div class="card-body p-2">
                      <p class="card-text">৩-০৩-২০১৯ ১৭:৩৯:৪৭</p>
                      <p class="card-text">কোহিনূর বেগম</p>
                      <p class="card-text">অফিস সহকারী, ব্যবসা-বাণিজ্য শাখা</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </li>
          <li class="list-group-item list-group-item-light p-0 mb-3 text-danger">
            <div class="d-flex justify-content-between align-items-center border-bottom px-2 py-1 onucched_heading">
              <h5 class="mb-0">অনুচ্ছেদ ১.০
                <small>(১৯-০২-২০১৯ ১২:১৮:৪৯ অপরাহ্ণ)</small>
              </h5>
              <div class="btn-group " role="group" aria-label="Basic example">
                <button type="button"
                    class="btn btn-label-primary btn-sm shadow-none btn-icon">
                  <i class="fs1 a2i_nt_dakdraft4"></i>
                </button>
                <button type="button" class="btn btn-label-success btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_edit2"></i>
                </button>
                <button type="button" class="btn btn-label-danger btn-sm  btn-icon">
                  <i class="fs1 a2i_gn_delete2 "></i>
                </button>
              </div>
            </div>
            <div class="p-2 bg-white">
              <div class="printArea">
                <div class="note_creation_time">১৯-০২-২০১৯ ০৪:১৯:০৫ অপরাহ্ণ</div>
                <span class="noteNo"> অনুচ্ছেদ: ১.১</span>
                <div class="noteDescription" id="114">বিকল্প প্রস্তাব দিন পরিচ্ছন্ন পত্রসহ উপস্থাপন করুন
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>

    </div>
  </div>

</div>
<!--onucched:end-->
            
