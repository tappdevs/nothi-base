<div class="promo-block" id="home">
    <div id="carousel-example-generic" class="carousel slide carousel-slider" style="margin-top: 60px;">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <!-- First slide -->
            <div class="item carousel-item-one active">
                <div class="center-block">
                    <div class="center-block-wrap">
                        <div class="center-block-body">
                            <h2 class="margin-bottom-20 animate-delay carousel-title-v1" data-animation="animated fadeInDown">
                                Parallax <span class="color-red">One Page</span> has arrived
                            </h2>
                            <div class="animated flipInX">
                                <div class="hidden-xs">
                                    <i class="promo-like fa fa-thumbs-up"></i>
                                    <div class="promo-like-text">
                                        <h2>Let's just do it</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing<br> elit amet sed diam nonummy nibh <a href="javascript:void(0);">dolore</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Second slide -->
            <div class="item carousel-item-two">
                <h2 class="carousel-position-one animate-delay carousel-title-v1" data-animation="animated fadeInDown">
                    Extremely <span class="color-red">Responsive</span> design
                </h2>
                <img class="carousel-position-two hidden-sm hidden-xs animate-delay" src="<?= $this->request->webroot; ?>assets/web/onepage/img/slider/Slide2_iphone_left.png" alt="Iphone" data-animation="animated fadeInUp">
                <img class="carousel-position-three hidden-sm hidden-xs animate-delay" src="<?= $this->request->webroot; ?>assets/web/onepage/img/slider/Slide2_iphone_right.png" alt="Iphone" data-animation="animated fadeInUp">
            </div>

            <!-- Third slide -->
            <div class="item carousel-item-three">
                <div class="center-block">
                    <div class="center-block-wrap">
                        <div class="center-block-body">
                            <h3 class="margin-bottom-20 animate-delay carousel-title-v2" data-animation="animated fadeInDown">
                                The clearest way into the Universe <br/> is through a forest wilderness.
                            </h3>
                            <span class="carousel-subtitle-v1">John Muir</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
</div>