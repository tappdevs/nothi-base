<div class="content " id="register">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Registration</h3>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <?= $this->Form->create($entity, ['class' => 'form-horizontal']) ?>
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">
                        Name
                    </label>
                    <div class="col-md-1">
                        <?= $this->Form->select('name_title',['options' => ['Mr.'=>'Mr.', 'Mrs.'=>'Mrs.', 'Ms.'=>'Ms.']], ['class' => 'form-control', 'required', 'label' => false]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('full_name', ['class' => 'form-control', 'placeholder' => 'Full Name', 'required', 'minLength' => 2, 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo __("Email");?></label>
                    <div class="col-md-4">
                        <?= $this->Form->input('email', ['class' => 'form-control email', 'placeholder' => 'Email', 'required', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo __("Mobile");?></label>
                    <div class="col-md-4">
                        <?= $this->Form->input('mobile', ['class' => 'form-control mobile', 'placeholder' => 'Mobile', 'required', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo __("Date of Birth");?></label>
                    <div class="col-md-4">
                        <?= $this->Form->input('date_of_birth', ['class' => 'form-control', 'placeholder' => 'Date of Birth', 'required', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo __("Gender");?></label>
                    <div class="col-md-4">
                        <?= $this->Form->select('gender',['options' => ['male'=>'Male', 'female'=>'Female', 'other'=>'Other']], ['class' => 'form-control', 'required', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo __("Password");?></label>
                    <div class="col-md-4">
                        <?= $this->Form->password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo __("Confirm Password");?></label>
                    <div class="col-md-4">
                        <?= $this->Form->password('cpassword', ['class' => 'form-control', 'placeholder' => 'Re-type Password', 'required', 'label' => false]) ?>
                        <span class="help-block"> Already member? <?= $this->Html->link('Login','login') ?> </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4  col-md-offset-3">
                    <?= $this->Flash->render() ?>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <?= $this->Form->button('Submit',['type'=>'submit','class'=>'btn green']) ?>
                        <?= $this->Form->button('Cancel',['type'=>'reset','class'=>'btn default']) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <!-- END FORM-->
        </div>
    </div>
</div>