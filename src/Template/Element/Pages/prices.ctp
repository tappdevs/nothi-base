<div class="prices-block content content-center" id="prices">
    <div class="container">
        <h2 class="margin-bottom-50"><strong>Sign Up</strong></h2>
        <div class="row">
            <!-- Pricing item BEGIN -->
            <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-3">
                <div class="pricing-item">
                    <div class="pricing-head">
                        <h3>Admin</h3>
                        <p>Lorem ipsum dolor</p>
                    </div>
                    <div class="pricing-content">

                    </div>
                    <div class="pricing-footer">
                        <a class="btn btn-default" href="<?= $this->Url->build(['_name'=>'signup',1]) ?>">Sign Up</a>
                    </div>
                </div>
            </div>
            <!-- Pricing item END -->
            <!-- Pricing item BEGIN -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="pricing-item">
                    <div class="pricing-head">
                        <h3>Student</h3>
                        <p>Lorem ipsum dolor</p>
                    </div>
                    <div class="pricing-content">

                    </div>
                    <div class="pricing-footer">
                        <a class="btn btn-default" href="<?= $this->Url->build(['_name'=>'signup',2]) ?>">Sign Up</a>
                    </div>
                </div>
            </div>
            <!-- Pricing item END -->
        </div>
    </div>
</div>