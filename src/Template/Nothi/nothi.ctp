<div class="d-flex h-100">
  <div class="row no-gutters flex-fill">
    <div id="note_left" class="h-100 d-flex flex-column">

      <div class="note_action my-2 mx-2">
        <button type="button"
            class="btn btn-outline-secondary w-100 justify-content-between d-flex btn-square card-toggle">
          <span><i class="fas fa-search"></i> নোট খুঁজুন</span>
          <i class="fa fa-caret-right"></i>
        </button>
        <div class="card shadow">
          <div class="card-header py-1 pr-1">
            <div class="d-flex justify-content-between align-items-center">
              <h5 class="mb-0">নোট খুঁজুন</h5>
              <button class="btn btn-danger btn-sm btn-icon card-close">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequuntur earum esse itaque optio recusandae
            voluptate. Accusamus accusantium distinctio dolore, eligendi, in ipsam minus odit quod repellat repudiandae
            vel, voluptatem?
          </div>
        </div>
      </div>
      <div class="note_action mb-2 mx-2">
        <button type="button"
            class="btn btn-outline-secondary w-100 justify-content-between d-flex btn-square card-toggle">
          <span><i class="far fa-list-alt"></i> সকল নোট</span>
          <i class="fa fa-caret-right"></i>
        </button>
        <div class="card shadow">
          <div class="card-header py-1 pr-1">
            <div class="d-flex justify-content-between align-items-center">
              <h5 class="mb-0">সকল নোট</h5>
              <button class="btn btn-danger btn-sm btn-icon card-close">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequuntur earum esse itaque optio recusandae
            voluptate. Accusamus accusantium distinctio dolore, eligendi, in ipsam minus odit quod repellat repudiandae
            vel, voluptatem?
          </div>
        </div>
      </div>
      <div class="note_action mb-2 mx-2">
        <button type="button"
            class="btn btn-outline-secondary w-100 justify-content-between d-flex btn-square card-toggle">
          <span><i class="far fa-file-alt"></i> নতুন নোট</span>
          <i class="fa fa-caret-right"></i>
        </button>
        <div class="card shadow">
          <div class="card-header py-1 pr-1">
            <div class="d-flex justify-content-between align-items-center">
              <h5 class="mb-0">নতুন নোট</h5>
              <button class="btn btn-danger btn-sm btn-icon card-close">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequuntur earum esse itaque optio recusandae
            voluptate. Accusamus accusantium distinctio dolore, eligendi, in ipsam minus odit quod repellat repudiandae
            vel, voluptatem?
          </div>
        </div>
      </div>


      <div class="note_action mb-2 mx-2">
        <button class="btn btn-outline-warning w-100 justify-content-between d-flex btn-square card-toggle">
          <span>নিজ ডেস্ক (৯৯৯)</span> <i class="fa fa-caret-right"></i>
        </button>

        <div class="card shadow">
          <div class="card-header py-1 pr-1">
            <div class="d-flex justify-content-between align-items-center">
              <h5 class="mb-0">নিজ ডেস্ক (৯৯৯)</h5>
              <button class="btn btn-danger btn-sm btn-icon card-close">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequuntur earum esse itaque optio recusandae
            voluptate. Accusamus accusantium distinctio dolore, eligendi, in ipsam minus odit quod repellat repudiandae
            vel, voluptatem?
          </div>
        </div>
      </div>

      <div class="note_checklist flex-fill overflow-hidden" data-scroll="true">
        <ul class="list-group list-group-flush border-top">
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg activeBg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck2">
              <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck3">
              <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck4">
              <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck5">
              <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg activeBg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck2">
              <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck3">
              <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck4">
              <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck5">
              <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg activeBg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck2">
              <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck3">
              <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck4">
              <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck5">
              <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg activeBg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck2">
              <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck3">
              <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck4">
              <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck5">
              <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg activeBg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck2">
              <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck3">
              <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck4">
              <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck5">
              <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">অনুচ্ছেদ ১</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg activeBg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck2">
              <label class="custom-control-label" for="customCheck2">অনুচ্ছেদ ২</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck3">
              <label class="custom-control-label" for="customCheck3">অনুচ্ছেদ ৩</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck4">
              <label class="custom-control-label" for="customCheck4">অনুচ্ছেদ ৪</label>
            </div>
          </li>
          <li class="list-group-item list-item-bg">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck5">
              <label class="custom-control-label" for="customCheck5">অনুচ্ছেদ ৫</label>
            </div>
          </li>
        </ul>
      </div>

      <input type="text" placeholder="Search onucched" class="form-control alert-info rounded-0" autofocus>


    </div>
    <div id="notansho_wrap" class="h-100">
      <div id="notansho_top">
        <div class="card border-0 h-100 mb-0">
          <div class="card h-100 add_onucched">
            <div class="card-header p-1">
              <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-start align-items-center">

                  <button class="btn btn-secondary  btn-sm btn-icon btn-square togg-btn">
                    <i class="fas fa-list-alt"></i>
                  </button>
                  <h5 class="mb-0 ml-3">নোটাংশ</h5>

                </div>

                <div>
                  <button class="btn btn-secondary  btn-sm btn-icon btn-square">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <button class="btn btn-secondary  btn-sm btn-icon btn-square">
                    <i class="la la-print"></i>
                  </button>
                  <button class="btn btn-secondary  btn-sm btn-icon btn-square">
                    <i class="flaticon-arrows"></i>
                  </button>
                </div>

              </div>
            </div>
            <div class="card-body overflow-hidden p-0 d-flex flex-column">

              <ul class="nav nav-tabs mb-0" role="tablist">
                <li class="nav-item">
                  <a class="nav-link py-1 pr-1" data-toggle="tab" href="#kt_tabs_1_1" role="tab"
                      aria-selected="false">Messages
                    <button class="ml-3 btn-sm btn btn-transparent btn-sm btn-icon"><i
                          class="fas fa-external-link-alt"></i></button>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link py-1 pr-1 active" data-toggle="tab" href="#kt_tabs_1_3" role="tab"
                      aria-selected="true">Logs
                    <button class="ml-3 btn-sm btn btn-transparent btn-sm btn-icon"><i
                          class="fas fa-external-link-alt"></i></button>
                  </a>
                </li>
              </ul>

              <div class="tab-content flex-fill overflow-hidden">
                <div class="tab-pane h-100 active" id="kt_tabs_1_1" role="tabpanel">
                  <?= $this->element('Nothi/note/onucched'); ?>
                </div>
                <div class="tab-pane h-100" id="kt_tabs_1_3" role="tabpanel">
                  <?= $this->element('Nothi/note/onucched'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--note-top-->
      <div id="notansho_bottom">
        <div class="card h-100 add_onucched">
          <div class="card-body p-0">
            <div id='froalaEditor' class="froala-editor"></div>
          </div>
          <div class="card-footer p-1">
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
                <button class="btn btn-sm btn-success d-flex align-items-center mr-1" title="" data-toggle="tooltip"
                    data-original-title="সংরক্ষণ">
                  <i class="fa fa-save"></i> <span class="ml-1">সংরক্ষণ</span>
                </button>
                <button class="btn btn-sm btn-secondary d-flex align-items-center mr-1 onucched_add_attachment" title=""
                    data-toggle="tooltip" data-original-title="সংযুক্তি">
                  <i class="fa fa-paperclip"></i> <span class="ml-1">সংযুক্তি</span>
                </button>
                <button class="btn btn-sm btn-secondary d-flex align-items-center mr-1" title="" data-toggle="tooltip"
                    data-original-title="নতুন অনুচ্ছেদ">
                  <i class="fs1 a2i_gn_note2"></i> <span class="ml-1">নতুন অনুচ্ছেদ</span>
                </button>
                <button class="btn btn-sm btn-secondary d-flex align-items-center mr-1" title="" data-toggle="tooltip"
                    data-original-title="প্রেরণ">
                  <i class="fs1 a2i_gn_send2"></i> <span class="ml-1">প্রেরণ</span>
                </button>
                <button class="btn btn-sm btn-danger d-flex align-items-center cancle_onucched" title=""
                    data-toggle="tooltip" data-original-title="বাতিল করুন">
                  <i class="fs1 a2i_gn_close2"></i> <span class="ml-1">বাতিল করুন</span>
                </button>
              </div>

              <button class="btn btn-secondary  btn-sm btn-icon btn-square">
                <i class="flaticon-arrows"></i>
              </button>

            </div>
          </div>
        </div>
      </div>
      <!--note-bottom-->
    </div>
    <!--notansho-->
    <div id="note_right" class="h-100">
      <div class="card h-100 add_onucched">
        <div class="card-header py-1 pr-1">
          <div class="d-flex justify-content-between align-items-center">
            <h5 class="mb-0 ml-3">পত্রাংশ</h5>
            <div>
              <button class="btn btn-secondary  btn-sm btn-icon btn-square">
                <i class="fa fa-file-pdf"></i>
              </button>
              <button class="btn btn-secondary  btn-sm btn-icon btn-square">
                <i class="la la-print"></i>
              </button>
              <button class="btn btn-secondary  btn-sm btn-icon btn-square">
                <i class="flaticon-arrows"></i>
              </button>
            </div>

          </div>
        </div>
        <div class="card-body p-0 d-flex flex-column">
          <ul class="nav nav-tabs mb-0" role="tablist">
            <li class="nav-item">
              <a class="nav-link active d-flex align-items-center" data-toggle="tab" href="#potro_tab_1" role="tab"
                  aria-controls="home">
                <i class="fa fa-inbox"></i>
                <aside class="ml-3 line-height-1">
                  <strong class="text-dark font-weight-normal">খসড়া</strong>
                  <span class="badge alert-warning p-1">12</span>
                </aside>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#potro_tab_2" role="tab"
                  aria-controls="profile">
                <i class="far fa-paper-plane"></i>
                <aside class="ml-3 line-height-1">
                  <strong class="text-dark font-weight-normal">নোটের পত্র</strong>
                  <span class="badge alert-danger p-1">232</span>
                </aside>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#potro_tab_3" role="tab"
                  aria-controls="messages">
                <i class="fas fa-envelope-open-text"></i>
                <aside class="ml-3 line-height-1">
                  <strong class="text-dark font-weight-normal">সকল পত্র</strong>
                  <span class="badge alert-success p-1">23</span>
                </aside>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#potro_tab_4" role="tab"
                  aria-controls="messages">
                <i class="fas fa-layer-group"></i>
                <aside class="ml-3 line-height-1">
                  <strong class="text-dark font-weight-normal">নথিজাত পত্র</strong>
                  <span class="badge alert-info p-1">23</span>
                </aside>
              </a>
            </li>
          </ul>




          <?= $this->element('Nothi/Note/potransho_navigator', ['potros_nav_id' => 'nav_khoshra_porto', 'potros_group_nav' => $potros_group_nav_all]);
          ?>

          <div class="potroview tab-content flex-fill overflow-hidden">
            <div class="tab-pane h-100 active" id="potro_tab_1" role="tabpanel">

              <div id="modal-containers" class="h-100">
                <div id="docxjs-wrapper" style="width:100%;height:100%;" data-url=""></div>
              </div>
            </div>
            <div class="tab-pane h-100" id="potro_tab_2" role="tabpane2">
              potro_tab_2
            </div>
            <div class="tab-pane h-100" id="potro_tab_3" role="tabpane3">
              potro_tab_3
            </div>
            <div class="tab-pane h-100" id="potro_tab_4" role="tabpane4">
              potro_tab_4
            </div>
          </div>




        </div>
      </div>
    </div>
    <!--potransho-->
  </div>
</div>

<!-- Include Editor style. -->
<link href='./node_modules/froala-editor/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css'/>

<!-- Include JS file. -->
<script type='text/javascript' src='./node_modules/froala-editor/js/froala_editor.pkgd.min.js'></script>
<script src="<?= $this->request->webroot ?>js/split.min.js"></script>


<script type="text/javascript" src="<?= $this->request->webroot ?>kukudocs/docxjs/DocxJS.bundle.min.js"></script>
<script type="text/javascript" src="<?= $this->request->webroot ?>kukudocs/celljs/CellJS.bundle.min.js"></script>
<script type="text/javascript" src="<?= $this->request->webroot ?>kukudocs/slidejs/SlideJS.bundle.min.js"></script>
<script type="text/javascript" src="<?= $this->request->webroot ?>kukudocs/pdfjs/PdfJS.bundle.min.js"></script>

<script>
  $(function (e) {

  })

</script>
<script>
  $(window).on("load", function (e) {
    var $body = $("body");

    var $loading = $("#parser-loading");
    var $modal = $("#modal");
    var $docxjsWrapper = $("#docxjs-wrapper");

    var instance = null;

    var getInstanceOfFileType = function (file) {
      var fileExtension = null;

      if (file) {
        var fileName = file.name;
        fileExtension = fileName.split(".").pop();
      }

      return fileExtension;
    };

    var documentParser = function (file) {
      var fileType = getInstanceOfFileType(file);

      if (fileType) {
        if (fileType == "docx") {
          instance = window.docxJS = window.createDocxJS ?
            window.createDocxJS() :
            new window.DocxJS();
        } else if (fileType == "xlsx") {
          instance = window.cellJS = window.createCellJS ?
            window.createCellJS() :
            new window.CellJS();
        } else if (fileType == "pptx") {
          instance = window.slideJS = window.createSlideJS ?
            window.createSlideJS() :
            new window.SlideJS();
        } else if (fileType == "pdf") {
          instance = window.pdfJS = window.createPdfJS ?
            window.createPdfJS() :
            new window.PdfJS();
          instance.setCMapUrl("./cmaps/");
        }

        if (instance) {
          $loading.show();
          instance.parse(
            file,
            function () {
              $docxjsWrapper[0].filename = file.name;
              afterRender(file, fileType);
              $loading.hide();
            },
            function (e) {
              if (!$body.hasClass("is-docxjs-rendered")) {
                $docxjsWrapper.hide();
              }

              if (e.isError && e.msg) {
                alert(e.msg);
              }

              $loading.hide();
            },
            null
          );
        }
      }
    };

    var afterRender = function (file, fileType) {
      var element = $docxjsWrapper[0];
      $(element).css("height", "calc(100% - 65px)");

      var loadingNode = document.createElement("div");
      loadingNode.setAttribute("class", "docx-loading");
      element.parentNode.insertBefore(loadingNode, element);
      $modal.show();

      var endCallBackFn = function (result) {
        if (result.isError) {
          if (!$body.hasClass("is-docxjs-rendered")) {
            $docxjsWrapper.hide();
            $body.removeClass("is-docxjs-rendered");
            element.innerHTML = "";

            $modal.hide();
            $body.addClass("rendered");
          }
        } else {
          $body.addClass("is-docxjs-rendered");
          // console.log("Success Render");
        }

        loadingNode.parentNode.removeChild(loadingNode);
      };

      if (fileType === "docx") {
        window.docxAfterRender(element, endCallBackFn);
      } else if (fileType === "xlsx") {
        window.cellAfterRender(element, endCallBackFn);
      } else if (fileType === "pptx") {
        window.slideAfterRender(element, endCallBackFn, 0);
      } else if (fileType === "pdf") {
        window.pdfAfterRender(element, endCallBackFn, 0);
      }
    };

    // var loadFirst = $('.load_docs input:first-child');

    // var loadURL = loadFirst.data('url');
    var loadURL = './docs/E-NothiRFP.pdf';
    // var loadID = loadFirst.data('id');
    // var loadNextID = loadFirst.next().data('id');
    // var loadNext = loadFirst.next().data('url');
    //
    // $('#docxjs-wrapper').attr('data-url', loadURL).attr('data-id', loadID);
    // $('.doc-next').attr('data-url', loadNext).attr('data-id', loadNextID);

    var xhr = new XMLHttpRequest();
    xhr.open("GET", loadURL);
    xhr.responseType = "blob";
    xhr.addEventListener("load", function () {
      xhr.response.name = loadURL;
      documentParser(xhr.response);
    });
    console.log(xhr);
    xhr.send();
  });


  $(function (e) {

    $('.doc-next').on('click', function (e) {

      $('#modal-containers').html('<div id="docxjs-wrapper"></div>');

      var getInstanceOfFileType = function (file) {
        var fileExtension = null;

        if (file) {
          var fileName = file.name;
          fileExtension = fileName.split(".").pop();
        }

        return fileExtension;
      };

      var documentParser = function (file) {
        var fileType = getInstanceOfFileType(file);

        if (fileType) {
          if (fileType == "docx") {
            instance = window.docxJS = window.createDocxJS ?
              window.createDocxJS() :
              new window.DocxJS();
          } else if (fileType == "xlsx") {
            instance = window.cellJS = window.createCellJS ?
              window.createCellJS() :
              new window.CellJS();
          } else if (fileType == "pptx") {
            instance = window.slideJS = window.createSlideJS ?
              window.createSlideJS() :
              new window.SlideJS();
          } else if (fileType == "pdf") {
            instance = window.pdfJS = window.createPdfJS ?
              window.createPdfJS() :
              new window.PdfJS();
            instance.setCMapUrl("./cmaps/");
          }

          if (instance) {
            $loading.show();
            instance.parse(
              file,
              function () {
                $docxjsWrapper[0].filename = file.name;
                afterRender(file, fileType);
                $loading.hide();
              },
              function (e) {
                if (!$body.hasClass("is-docxjs-rendered")) {
                  $docxjsWrapper.hide();
                }

                if (e.isError && e.msg) {
                  alert(e.msg);
                }

                $loading.hide();
              },
              null
            );
          }
        }
      };

      var afterRender = function (file, fileType) {
        var element = $docxjsWrapper[0];
        $(element).css("height", "calc(100% - 65px)");

        var loadingNode = document.createElement("div");
        loadingNode.setAttribute("class", "docx-loading");
        element.parentNode.insertBefore(loadingNode, element);
        $modal.show();

        var endCallBackFn = function (result) {
          if (result.isError) {
            if (!$body.hasClass("is-docxjs-rendered")) {
              $docxjsWrapper.hide();
              $body.removeClass("is-docxjs-rendered");
              element.innerHTML = "";

              $modal.hide();
              $body.addClass("rendered");
            }
          } else {
            $body.addClass("is-docxjs-rendered");
            // console.log("Success Render");
          }

          loadingNode.parentNode.removeChild(loadingNode);
        };

        if (fileType === "docx") {
          window.docxAfterRender(element, endCallBackFn);
        } else if (fileType === "xlsx") {
          window.cellAfterRender(element, endCallBackFn);
        } else if (fileType === "pptx") {
          window.slideAfterRender(element, endCallBackFn, 0);
        } else if (fileType === "pdf") {
          window.pdfAfterRender(element, endCallBackFn, 0);
        }
      };


      var $body = $("body");

      var $loading = $("#parser-loading");
      var $modal = $("#modal");
      var $docxjsWrapper = $("#docxjs-wrapper");

      var instance = null;

      var loadID = $(this).data('id');

      var loadFirst = $('.load_docs').find('input[data-id="' + loadID + '"]');

      var loadURL = loadFirst.attr('data-url');

      var loadPreviusID = loadFirst.prev().data('id');
      var loadNextID = loadFirst.next().data('id');

      var loadPreviusUrl = loadFirst.prev().data('url');
      var loadNextUrl = loadFirst.next().data('url');

      console.log(loadID);

      // return false;


      $('#docxjs-wrapper').attr('data-url', loadURL);

      $('.doc-previus').attr('data-id', loadPreviusID).attr('data-url', loadPreviusUrl);
      $('.doc-next').attr('data-id', loadNextID).attr('data-url', loadNextUrl);

      var xhr = new XMLHttpRequest();
      xhr.open("GET", loadURL);
      xhr.responseType = "blob";
      xhr.addEventListener("load", function () {
        xhr.response.name = loadURL;
        documentParser(xhr.response);
      });
      console.log(xhr);
      xhr.send();


    })
  })
</script>

<script>

  // Initialize editor.
  new FroalaEditor('#froalaEditor', {
    key: 'xc1We1KYi1Ta1WId1CVd1F=='
  });


  (function (e) {


    $('.card-toggle').each(function (e) {
      var cartToggler = $(this);
      cartToggler.on('click', function (e) {
        $('.show').removeClass('show');
        $(this).siblings('.card').addClass('show');
        e.stopPropagation();
      })
      $(document).on('click', function (e) {
        $(this).find('.card').removeClass('show');
        e.stopPropagation();
      })
      $('.card-close').on('click', function (e) {
        $(this).parents('.card').removeClass('show');
        e.stopPropagation();
      })
      $('.card.show').on('click', function (e) {
        e.stopPropagation();
      })

    })

    function toggleArea(clickBtn, areaToggle) {
      var clickBtn = $(clickBtn);
      var areaToggle = $(areaToggle);
      clickBtn.on('click', function (e) {
        areaToggle.toggleClass('show');
        e.stopPropagation();
      })
      $('.togg-close').on('click', function (e) {
        areaToggle.removeClass('show');
        e.stopPropagation();
      })
    }

    function toggleAreaHide(clickBtn, areaToggle) {
      var clickBtn = $(clickBtn);
      var areaToggle = $(areaToggle);
      clickBtn.on('click', function (e) {
        areaToggle.toggleClass('show');
        e.stopPropagation();
      })
      $(document).on('click', function (e) {
        areaToggle.removeClass('show');
        e.stopPropagation();
      })
      $('.togg-close').on('click', function (e) {
        areaToggle.removeClass('show');
        e.stopPropagation();
      })
      areaToggle.on('click', function (e) {
        e.stopPropagation();
      })
    }

    toggleArea('.togg-btn', '.onucched_numbers');


    if ($('#note_left, #notansho_wrap, #note_right').length) {
      var noteTopBottom = Split(['#note_left', '#notansho_wrap', '#note_right'], {
        sizes: [10, 45, 45],
        minSize: [150, 300, 300],
        direction: 'horizontal'
      });
      // noteTopBottom.collapse(1);
    }

    if ($('#note_left, #notansho_wrap, #note_right').length) {
      var noteTopBottom = Split(['#notansho_top', '#notansho_bottom'], {
        sizes: [70, 30],
        direction: 'vertical'
      });
      // noteTopBottom.collapse(1);

    }
  })(jQuery)
</script>
