
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Registration">
    <meta name="author" content="Tappware">
    <meta name="keyword" content="tappware">
    <title>Registration</title>

<!--    <link href="vendors/css/coreui-icons.min.css" rel="stylesheet">-->
    <link href="vendors/css/flag-icon.min.css" rel="stylesheet">
    <link href="vendors/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/css/simple-line-icons.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
<!-- Global js BEGIN -->
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<div class="container-fluid">
    <div id="ui-view">
        <?=$this->Flash->render();?>
        <?= $this->fetch('content') ?>
    </div>
</div>
<script src="vendors/js/jquery.min.js"></script>
<script src="vendors/js/popper.min.js"></script>
<script src="vendors/js/bootstrap.min.js"></script>

</body>
</html>
